/*************************************************************************
 *
 * Function: Community Sift Content Moderation API Integration
 * Description: Use content filtering to detect potential profanity and flag text that may be deemed inappropriate depending on context (in public preview), and match text against your custom lists.
 * Author: PubNub
 * Categories: External APIs, Chat & Social
 * More Information: https://www.twohat.com/community-sift
 *
 * SETUP: 
 *   1. Create a Secret in PubNub Vault, named COMMUNITY_SIFT_APIKEY, with value equal to
 *      your Community Sift API Key.
 *   2. Update the API_URI to point to your Community Sift content moderation resource
 *   3. OPTIONAL: Update PubNub Channel to use for broadcasting profanity events (useful for moderation).
 *     If value is left empty then, no other publish action will occur.
 *   4. OPTIONAL: change BLOCK_PUBLISH to true if recommended for moderation content should be rejected
 *************************************************************************/
export default (request) => {
    const pubnub = require('pubnub');

    if (request.channels[0].endsWith('-pnpres')) {
        return request.ok();
    }
    const function_path = 'communitysift-moderate-text';

    // allow for per message control, e.g. use same function for differently shaped message payloads by specifying input parameters as part of the `meta` property of the publish
    let t_meta = request.params.meta !== undefined ? JSON.parse(request.params.meta) : {}; 
    let per_message_control = t_meta.functions !== undefined && t_meta.functions[function_path] !== undefined && t_meta.functions[function_path].input !== undefined ? t_meta.functions[function_path].input : {};
    console.log("prev-message:"+JSON.stringify(request.message));
    // this will call the `On Request` function; this does not mean that call goes outside of PubNub network so latency is fairly low;  the target function can be leveraged 
    // as `Before Publish or Fire` through some simple modification of code
    if (request.message.payload.text === undefined) {
        console.log("not text action");
        return request.ok();
    }
    
    let objMessage = { "content": { "type": request.message.type , "body": request.message.payload.text }, "sender": request.message.payload.uuid };
    console.log("proc-message:"+ JSON.stringify(objMessage));
    
    
    return pubnub.triggerRequestFunction({path: function_path, method:'POST', headers:{},query:per_message_control,body: objMessage})
    .then((serverResponse) => {
        const body = JSON.parse(serverResponse.body);
        console.log("body:"+ serverResponse.body);
        if (t_meta.functions !== undefined && t_meta.functions[function_path] !== undefined  && t_meta.functions[function_path].input !== undefined && t_meta.functions[function_path].input['OUTPUT_LOCATION_ID'] !== undefined  && t_meta.functions[function_path].input['OUTPUT_LOCATION_ID'] === 'meta') {
            // put output in the meta property if you do not want to store in message history
            t_meta.functions[function_path].output = body; 
            request.params.meta = JSON.stringify(t_meta);
            console.log("Meta:"+request.params.meta);
        } else {
            // output from function will be included in message body and can be stored along with the message
            request.message = Object.assign(request.message, body);
            console.log("MESSAGE:"+ JSON.stringify(request.message));
            console.log("IS OK?:"+ JSON.stringify(request.message.sentiment.sift.response));
            // TODO aqui falta ver que hacemos... cuando rechazamos
            // de momento solo utilizo el criterio response
            if (request.message.sentiment.sift.response === false) {
                request.message.payload.text = request.message.sentiment.sift.hashed;
            }
        }
        console.log('REQUEST OK');
        return request.ok(); // Return a promise when you're done 
    }).catch((err) => {
        // handle request failure
        console.log('MODERATION FUNCTION FAILURE :: ' + err);
        return request.abort(); // Return a promise when you're done 
    });

};