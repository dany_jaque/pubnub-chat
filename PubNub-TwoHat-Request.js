/*************************************************************************
 *
 * Function: Community Sift Content Moderation API Integration
 * Description: Use content filtering to detect potential profanity and flag text that may be deemed inappropriate depending on context (in public preview), and match text against your custom lists.
 * Author: PubNub
 * Categories: External APIs, Chat & Social
 * More Information: https://www.twohat.com/community-sift
 *
 * SETUP: 
 *   1. Create a Secret in PubNub Vault, named COMMUNITY_SIFT_APIKEY, with value equal to
 *      your Community Sift API Key.
 *   2. Update the API_URI to point to your Community Sift content moderation resource
 *   3. OPTIONAL: Update PubNub Channel to use for broadcasting profanity events (useful for moderation).
 *     If value is left empty then, no other publish action will occur. NOT USED; FUTURE
 *   4. OPTIONAL: change BLOCK_PUBLISH to true if recommended for moderation content should be rejected
 *************************************************************************/
 export default (request, response) => {
    const pubnub = require('pubnub');
    const vault = require('vault');
    const xhr = require('xhr');
    const db = require("kvstore");
    const basicAuth = require('codec/auth');

    const function_path = 'communitysift-moderate-text';

    const functions_config = {
        env: {
            'API_URI': 'https://apidemo.communitysift.com',
            'API_PATH': '/v1/chat',
            'API_METHOD': 'POST',
            'API_KEY_IDENTIFIER': 'COMMUNITY_SIFT_APIKEY',
            'TEXT_LOCATION_ID': 'content.body',
            'OUTPUT_LOCATION_ID': 'sentiment.sift',    
            //'PN_PROFANITY_DETECTED_CHANNEL': 'profanity_detected', // if left empty publish to a side moderation channel won't happen
            'BLOCK_PUBLISH': false // set to false if all inappropriate content should be blocked
        },
        secrets: ['COMMUNITY_SIFT_APIKEY'],
        logger: {}

    };

    const env = (env_var_name) => {
        let env = functions_config.env[env_var_name];
        if (env !== null || env !== undefined) {
            return env;
        } 
        
        throw new Error('No env value exists with that name');
    }

    const moderate_if_needed = (message, classificationData, PN_PROFANITY_DETECTED_CHANNEL) => {
        return new Promise((resolve, reject) => {
            if (classificationData.false === false && PN_PROFANITY_DETECTED_CHANNEL !== '')  {
                resolve(pubnub.publish({'channel': PN_PROFANITY_DETECTED_CHANNEL, 'message': message}));
            } else {
                resolve(true);
            }
        });
    }        

    const params = request.params;
    const body = JSON.parse(request.body);

    const API_URI = env('API_URI');
    const API_PATH = env('API_PATH');
    const API_METHOD = env('API_METHOD');
    const API_KEY_IDENTIFIER = env('API_KEY_IDENTIFIER');
    const TEXT_LOCATION_ID ='TEXT_LOCATION_ID' in params ? params['TEXT_LOCATION_ID']:  env('TEXT_LOCATION_ID');
    const API_RESPONSE_ELEMENT = env('API_RESPONSE_ELEMENT');
    const OUTPUT_LOCATION_ID  = 'OUTPUT_LOCATION_ID' in params ? params['OUTPUT_LOCATION_ID']:  env('OUTPUT_LOCATION_ID');
    //const PN_PROFANITY_DETECTED_CHANNEL = env('PN_PROFANITY_DETECTED_CHANNEL');
    const BLOCK_PUBLISH = env('BLOCK_PUBLISH');
    

    /**
     * COMMON LOGIC
     * 
    */
    
    /**
     * Helper function to make error propagation easier by generating a standardized error object
     * @param statusCode - HTTP status code to return
     * @param functionErrorMessage - Function level error message, will be returned as a top-level property
     * @returns {{functionErrorMessage: *, statusCode: *}}
     */
    const makeError = (statusCode, functionErrorMessage) => {
        return { statusCode, functionErrorMessage };
    };

    /**
     * Helper function to make error propagation easier by generating a standardized error object
     * @param statusCode - HTTP status code to return
     * @param functionErrorMessage - Function level error message
     * @param apiErrorResponse - An error response object from an API call
     * @returns {{functionErrorMessage: *, apiErrorResponse: *, statusCode: *}}
     */
    const makeApiError = (statusCode, functionErrorMessage, apiErrorResponse) => {
        //We have to substring the error message as there is a log size limit for functions
        /*console.log(apiErrorResponse);
        console.error(
            "Got error from API:",
            JSON.stringify(apiErrorResponse, Object.getOwnPropertyNames(apiErrorResponse)).substring(0, 1000)
        );*/
        return { statusCode, functionErrorMessage, apiErrorResponse };
    };

    /**
     * Called when an error response should be returned to the function caller
     * @param error - Error details object. If the object contains the property 'functionErrorMessage',
     * this value will be used as the main error. Otherwise, the error object will be stringified and used instead.
     */
    const handleError = (error) => {
        console.error("Responding with error:", JSON.stringify(error, Object.getOwnPropertyNames(error)));
        response.status = error.statusCode;
        return response.send(error.apiErrorResponse);
    };

    /**
     * Called when building a response object with a specific output object required
     * 
    */
    const createObject = function(key, value) {
        var obj = {};
        var parts = key.split('.');
        if(parts.length == 1) {
            obj[parts[0]] = value;
        } else if(parts.length > 1) {
            var remainingParts = parts.slice(1,parts.length).join('.');
            obj[parts[0]] = createObject(remainingParts, value);
        }
        return obj;  
    };
    

    /**
     * Get a secret from the PubNub Functions Vault
     * @param vaultKey - The vault secret key
     * @returns {Promise<String>} - The vault secret value
     */
    const getVaultSecret = (vaultKey) => {
        return new Promise((resolve, reject) => {
            vault.get(vaultKey)
                .then(resolve)
                .catch(vaultError => {
                    reject(makeError(
                        500,
                        "Vault '" + vaultKey + "' get failed. Error details: " + vaultError
                    ));
                });
        });
    };

    /**
     * CUSTOM LOGIC
     * 
    */

    /**
     * Called if the API call was successful
     * @param apiStatusCode - HTTP code returned from the API call
     * @param apiResponseBody - JSON response received from the API call
     */
    const handleSuccess = (apiStatusCode, apiResponseBody) => {
        return new Promise((resolve, reject) => {
            response.status = apiStatusCode;
            if (OUTPUT_LOCATION_ID !== undefined && OUTPUT_LOCATION_ID !== '' && OUTPUT_LOCATION_ID !== '.') {
                apiResponseBody = createObject(OUTPUT_LOCATION_ID, apiResponseBody);
            }

            if (BLOCK_PUBLISH) {
                reject(makeApiError(403, "Forbidden", "Inappropriate content submitted."));
            } else {
                response.send(apiResponseBody).then(resolve, reject);
            }
            
        });
    };
    
    /**
     * Will call the third-party API when called. Returns a promise containing the API response.
     *
     * @param apiKey
     * @param requestBody
     * @returns {Promise<String>} - Stringified JSON response
     */
    const makeRequest = (apiKey, requestBody) => {
        return new Promise((resolve, reject) => {
            let headers = {};
            let opts    = {
                host    : API_URI
            ,   method  : API_METHOD
            ,   path    : API_PATH
            ,   headers : headers
            ,   body    : requestBody
            };

            headers['Connection'] = 'Keep-Alive';
            headers['Content-Type'] = 'application/json';
            headers['Authorization'] = basicAuth.basic('demo', apiKey);

            
            let uri = opts.host + opts.path;
            console.log("URI:"+ uri);
            console.log("BODY:"+ JSON.stringify(requestBody));
            xhr.fetch(
                uri,
                opts
            ).then(apiResponse => {
                //We have to substring the response as there is a log size limit for functions
                console.log("Received response:", JSON.stringify(apiResponse).substring(0, 1000));
                //TODO other functions should do this
                //We have to check the response for a status that is not 2XX
                if (apiResponse.ok) {
                    resolve(apiResponse);
                } else {
                    reject(makeApiError(apiResponse.status, "Error response from API", JSON.parse(apiResponse.body)));
                }
            }).catch(apiError => {
                reject(makeApiError(apiError.status, "Error calling API", apiError));
            });
        });
    };

    
    /**
     * Gets the text to analyzie based on specified location
     * Returns a string
     *
     * @param message
     * @returns String
     */
    const getContent = (message) => {
        let arr = TEXT_LOCATION_ID.split(".");
        return arr.length === 1 ? (arr[0] in message ? message[arr[0]] : null)
            :  arr.length === 2 ? (arr[0] in message && arr[1] in message[arr[0]] ? message[arr[0]][arr[1]] : null)
            : arr.length === 3 ? (arr[0] in message && arr[1] in message[arr[0]] && arr[2] in message[arr[0]][arr[1]] ? message[arr[0]][arr[1]][arr[3]]: null)
            : null;
    }
    
    /**
     * Constructs the API request body using the parameters provided in the function request.
     * Returns a promise containing the API response.
     *
     * @param apiKey
     * @returns {Promise<String>} - Stringified JSON response
     */
    const callApi = (apiKey) => {
        return new Promise((resolve, reject) => {
            let content = getContent(body);
            console.log("This is the content:"+ content);
            if (content !== null) {
                const document = {text: content};
                const makeApiRequest = (requestBody) => makeRequest(apiKey, requestBody);
                makeApiRequest(document).then(resolve, reject);
            } else {
                reject(makeError(400, "Missing required parameters"));
            }
        });
    };

    /**
     *  Main - PubNub On Request Functions expect a promise to be returned that resolves to a response object.
     *  This method therefore returns a series of nested asynchronous calls that will retrieve PubNub vault secrets
     *  and call the third-party API.
     */
    return getVaultSecret(API_KEY_IDENTIFIER).then(apiKey => {
        return callApi(apiKey).then(apiResponse => {
            return handleSuccess(apiResponse.status, JSON.parse(apiResponse.body));
        }).catch(handleError);
    }).catch(handleError);
    
};